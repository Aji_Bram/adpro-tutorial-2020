package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
        //ToDo: Complete me
        @Override
        public String getType() {
                return "Armorer";
        }

        @Override
        public String defend() {
                return "Covered with the strongest armor!";
        }
}
