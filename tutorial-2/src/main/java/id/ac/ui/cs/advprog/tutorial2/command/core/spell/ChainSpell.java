package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    protected ArrayList<Spell> chain;

    public ChainSpell (ArrayList<Spell> chain){
        this.chain = chain;
    }

    @Override
    public void cast() {
        for (Spell chains : chain){
            chains.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = chain.size()-1 ; i >= 0; i--){
            chain.get(i).undo();
        }
    }


    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
